+++
draft = false
comments = false
slug = ""
tags = []
categories = ["about"]
title = "About"

showpagemeta = false
showcomments = false
+++

I did my PhD in Paris, France, under supervision of [Anuradha Jagannathan](https://www.equipes.lps.u-psud.fr/Jagannathan/), and I then moved to Toulouse (south of France) for a post-doc with [Fabien Alet](http://www.lpt.ups-tlse.fr/spip.php?article20) and [Nicolas Laflorencie](http://www.lpt.ups-tlse.fr/spip.php?article53).

I also play the [clarinet](https://youtu.be/-Oqrw93E1_g), the [soprano sax](https://youtu.be/FLvJWxeZJH0) and various [recorders](https://youtu.be/qnIB_BDPU_w) *(click on each instrument for a piece I like)*.
