+++
draft = false
comments = false
slug = ""
tags = []
categories = ["publications"]
title = "Publications"

showpagemeta = false
showcomments = false
+++

See my preprints on [arXiv.org](https://arxiv.org/a/mace_n_1.html), or on [Google Scholar](https://scholar.google.fr/citations?user=Qi6hqOEAAAAJ&hl=en).
My ORCID is [0000-0002-2438-7524](https://orcid.org/0000-0002-2438-7524).

2018
----

+ **Multifractal scalings across the many-body localization transition**  
N. Macé, F. Alet and N. Laflorencie  
[arXiv:1812.10283](https://arxiv.org/abs/1812.10283)  

+ **Many-body localization in a quasiperiodic Fibonacci chain**  
N. Macé, N. Laflorencie and F. Alet  
[arXiv:1811.01912](https://arxiv.org/abs/1811.01912)  

+ **Shift-invert diagonalization of large many-body localizing spin chains**  
F. Pietracaprina, N. Macé, D. J. Luitz and F. Alet  
[*SciPost Phys.* **5**, 045 (2018)](https://scipost.org/submissions/1803.05395v3/) [arXiv:1803.05395](https://arxiv.org/abs/1803.05395)  
[Open access, source code available ([link](https://bitbucket.org/dluitz/sinvert_mbl/))]

2017
----
+ **Critical eigenstates and their properties in one- and two-dimensional quasicrystals**  
N. Macé, A. Jagannathan, P. Kalugin, R. Mosseri et F. Piéchon  
[*Phys. Rev. B* **96**, 045138 (2017)](https://link.aps.org/doi/10.1103/PhysRevB.96.045138) [arXiv:1706.06796](https://arxiv.org/abs/1706.06796)

+ **Gap structure of 1D cut and project Hamiltonians**  
N. Macé, A. Jagannathan and F. Piéchon  
[*J. Phys.: Conference Series* **809**(1) (2017)](http://stacks.iop.org/1742-6596/809/i=1/a=012023), [arXiv:1704.06463](https://arxiv.org/abs/1704.06463)  
[Open access, conference proceeding]

2016
----
+ **Quantum simulation of a 2D quasicrystal with cold atoms**  
N. Macé, A. Jagannathan and M. Duneau  
[*Crystals* **6(10)** (2016)](https://www.mdpi.com/2073-4352/6/10/124), [arXiv:1609.08509](https://arxiv.org/abs/1609.08509)  
[Open access, Review]

+ **Fractal dimensions of the wavefunctions and local spectral measures on the Fibonacci chain**  
N. Macé, A. Jagannathan and F. Piéchon  
[*Phys. Rev. B* **93**, 205153 (2016)](https://journals.aps.org/prb/abstract/10.1103/PhysRevB.93.205153), [arXiv:1601.00532](https://arxiv.org/abs/1601.00532)

PhD Thesis
----
**Electronic properties of quasicrystals**  
[link](https://framagit.org/Yukee/PhD_thesis/raw/master/main.pdf)
