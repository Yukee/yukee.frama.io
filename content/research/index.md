+++
draft = false
comments = false
slug = ""
tags = []
categories = ["research"]
title = "Research"

showpagemeta = false
showcomments = false
+++

Broadly speaking, I study the properties of condensed matter systems using analytical and numerical methods.

Floquet systems
----
<img src="/img/research/floquet/circuit.png" width="500" alt="A Floquet quantum circuit" title="A Floquet quantum circuit" />

[Under construction]

Many-body localization
----
<img src="/img/research/mbl/participation.png" width="600" alt="Extension of the eigenstates of a spin chain in their Hilbert space in a thermal (green) or an MBL (blue) phase" title="Extension of the eigenstates of a spin chain in their Hilbert space in a thermal (green) or an MBL (blue) phase" />

[Under construction]

Quasicrystals
----
<img src="/img/research/quasiperiodic/tim.png" width="600" alt="A multifractal eigenstate on the quasiperiodic Ammann-Beenker tiling" title="A multifractal eigenstate on the quasiperiodic Ammann-Beenker tiling" />
Quasicrystals are metallic alloys whose atoms arrange in a non-periodic yet highly ordered fashion.
They are notably famous for being highly symmetric under rotation (the figure features the eigenfold symmetric Ammann-Beenker tiling).
Owing to their non-periodicity, they escape the usual band theory of solids.
Instead, they feature some unusual mathematical beasts: their electronic density is a scale-invariant [multifractal object](https://en.wikipedia.org/wiki/Multifractal_system), and their integrated density of states can be a [Devil's staircase](https://en.wikipedia.org/wiki/Cantor_function).

My research focuses on understanding the fractality of quantum particles on quasiperiodic tilings from geometrical principles.
